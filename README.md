# Fab Zero: The Last Shangrila 

This is the sixth iteration of the Fab Zero cycle mentored by Fran Sanchez and Sibu Saman. 

Goals of this cycle:

- Set up 3 Fab Labs (Paro, Gelephu and Punkaha) & 1 Super Fab Lab (Thimphu)
- Learn the skills required to successfully compete a project using the machines in a Fab Lab. 
- Come up with a final project idea and complete it. 
- Enjoy and have fun! Try not to burn out.

## Content

- [Intro to Github](Add link here)
- Image editing and compression
- 

## Final Project 


## Project Status 

This Program will take place from January 25th to March 21st 2022. 

## Resources 

- [Fab Zero Gitlab page](https://gitlab.fabcloud.org/fabzero/fabzero/-/blob/master/program/basic/intro.md)
